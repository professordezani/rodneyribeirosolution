﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace RodneyRibeiro.Models
{
    public class User 
    {
        public int IdUsuario { get; set; }

        public int Status { get; set; }

        [Required(ErrorMessage = "O campo de email é obrigatório")]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo nome é obrigatório")]
        [DisplayName("Primeiro Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo sobrenome é obrigatório")]
        [DisplayName("Sobrenome")]
        public string Sobrenome { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DataNasc { get; set; }

        [Required(ErrorMessage = "O campo de senha é obrigatório")]
        [DisplayName("Senha")]
        public string Senha { get; set; }

        public decimal Saldo { get; set; }
        public decimal Despesa { get; set; }

        public int Lista_Id { get; set; }

        public int NumLinha1 { get; set; }
        public int NumLinha2 { get; set; }
        public int NumLinha3 { get; set; }
        public int NumLinha4 { get; set; }
        public int NumLinha5 { get; set; }
        public int NumLinha6 { get; set; }
        public int NumLinha7 { get; set; }
        public int NumLinha8 { get; set; }
        public int NumLinha9 { get; set; }
        public int NumLinha10 { get; set; }
        public int NumLinha11 { get; set; }
        public int NumLinha12 { get; set; }
        public int NumLinha13 { get; set; }
        public int NumLinha14 { get; set; }
        public int NumLinha15 { get; set; }

        public int Codigo1 { get; set; }
        public int Codigo2 { get; set; }
        public int Codigo3 { get; set; }
        public int Codigo4 { get; set; }
        public int Codigo5 { get; set; }
        public int Codigo6 { get; set; }
        public int Codigo7 { get; set; }
        public int Codigo8 { get; set; }
        public int Codigo9 { get; set; }
        public int Codigo10 { get; set; }
        public int Codigo11 { get; set; }
        public int Codigo12 { get; set; }
        public int Codigo13 { get; set; }
        public int Codigo14 { get; set; }
        public int Codigo15 { get; set; }

        public string Descricao1 { get; set; }
        public string Descricao2 { get; set; }
        public string Descricao3 { get; set; }
        public string Descricao4 { get; set; }
        public string Descricao5 { get; set; }
        public string Descricao6 { get; set; }
        public string Descricao7 { get; set; }
        public string Descricao8 { get; set; }
        public string Descricao9 { get; set; }
        public string Descricao10 { get; set; }
        public string Descricao11 { get; set; }
        public string Descricao12 { get; set; }
        public string Descricao13 { get; set; }
        public string Descricao14 { get; set; }
        public string Descricao15 { get; set; }

        public string Num1 { get; set; }
        public string Num2 { get; set; }
        public string Num3 { get; set; }
        public string Num4 { get; set; }
        public string Num5 { get; set; }
        public string Num6 { get; set; }
        public string Num7 { get; set; }
        public string Num8 { get; set; }
        public string Num9 { get; set; }
        public string Num10 { get; set; }
        public string Num11 { get; set; }
        public string Num12 { get; set; }
        public string Num13 { get; set; }
        public string Num14 { get; set; }
        public string Num15 { get; set; }

        public string Historico1 { get; set; }
        public string Historico2 { get; set; }
        public string Historico3 { get; set; }
        public string Historico4 { get; set; }
        public string Historico5 { get; set; }
        public string Historico6 { get; set; }
        public string Historico7 { get; set; }
        public string Historico8 { get; set; }
        public string Historico9 { get; set; }
        public string Historico10 { get; set; }
        public string Historico11 { get; set; }
        public string Historico12 { get; set; }
        public string Historico13 { get; set; }
        public string Historico14 { get; set; }
        public string Historico15 { get; set; }

        public string Entrada1 { get; set; }
        public string Entrada2 { get; set; }
        public string Entrada3 { get; set; }
        public string Entrada4 { get; set; }
        public string Entrada5 { get; set; }
        public string Entrada6 { get; set; }
        public string Entrada7 { get; set; }
        public string Entrada8 { get; set; }
        public string Entrada9 { get; set; }
        public string Entrada10 { get; set; }
        public string Entrada11 { get; set; }
        public string Entrada12 { get; set; }
        public string Entrada13 { get; set; }
        public string Entrada14 { get; set; }
        public string Entrada15 { get; set; }

        public string Saida1 { get; set; }
        public string Saida2 { get; set; }
        public string Saida3 { get; set; }
        public string Saida4 { get; set; }
        public string Saida5 { get; set; }
        public string Saida6 { get; set; }
        public string Saida7 { get; set; }
        public string Saida8 { get; set; }
        public string Saida9 { get; set; }
        public string Saida10 { get; set; }
        public string Saida11 { get; set; }
        public string Saida12 { get; set; }
        public string Saida13 { get; set; }
        public string Saida14 { get; set; }
        public string Saida15 { get; set; }
    }
}