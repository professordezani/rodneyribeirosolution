﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace RodneyRibeiro.Models
{
    public class ModelBase : IDisposable
    {
        protected SqlConnection connection;

        public ModelBase()
        {
            string strConn = "Data Source = localhost ; Initial Catalog = Aquarius; Integrated Security = true; MultipleActiveResultSets = true";
            connection = new SqlConnection(strConn);

            connection.Open();
        }

        public void Dispose()
        {
            connection.Close();
        }
    }
}